// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// code to provide the "readme" sliding popup on the site
// It can be commented out from index.html or repurposed when we release NOP

function help_page(page)
{
        var textA = '<h2>Commands:</h2>'+
                '<ul><li><b>walk @nodename</b> : move to a new location</li>'+
                '<li><b>look</b> : look at the current location</li>'+
                '<li><b>look @entityname</b> : look closely at an object</li>'+
                '<li><b>make entityname</b> : make a new object</li>'+
                '<li><b>take @entityname</b> : pickup an object</li>'+
                '<li><b>inventory</b> : see what you are carrying</li>'+
                '<li><b>drop @entityname</b> : drop an object</li>'+
                '<li><b>makebot botname</b> : make a new bot</li>'+
                '<li><b>program @botname slub</b> : make the bot programmable</li>'+
                '<li><b>remove @entityname</b> : remove a bot or entity</li>'+
                '<li><b>dress @entityname @clothesentityname</b> : dress a bot or entity with another one</li>'+
                '</ul>'+
                '<h2>Bot Commands:</h2>'+
                '<ul><li><b>@botname</b> @botname ping: get status info from the bot</li>'+
                '<li><b>@botname run</b> : start the bot running</li>'+
                '<li><b>@botname stop</b> : stop the bot running</li>'+ 
                '<li><b>@botname list</b> : get the bot to tell you it\'s code</li>'+ 
                '<li><b>@botname 10 say hello world</b> : program a line</li></ul>'
;

        var textB = '<h2>Entering an example program</h2>'+
                '<ul><li>@MyBot 10 say hello world</li>'+
                '<li>@MyBot 20 goto 10</li>'+
                '<li>@MyBot run</li></ul>'+
                '<h2>Doing stuff</h2>'+
                '<ul><li><b>say what to say</b> : speak</li>'+
                '<li><b>walk</b> : move to another node/place</li>'+
                '<li><b>pickup objectname</b> : pick up an object</li>'+
                '<li><b>drop objectname</b> : drop up an object</li>'+
                '<li><b>make objectname</b> : create a new object</li>'+
                '<li><b>dress objectname clothesobjectname</b> : dress n bot/player/entity</li>'+
                '</ul>'+
                '<h2>Looping and conditions</h2>'+
                '<ul>'+
                '<li><b>for name in path</b> : loop over stuff</li>'+
                '<li><b>if path is name</b> : conditional</li>'+
                '<li><b>if path isnot name</b> : conditional</li>'+
                '<li><b>if random lessthan 5</b> : random conditional (random is 0-9)</li>'+
                '<li><b>end</b> : end a loop or conditional</li></ul>'
            ;

    var textC = '<h2>Paths are routes into the game data</h2>'+
            '<ul>'+
            '<li><b>this</b> : the bot</li>'+
            '<li><b>node</b> : the current location</li>'+
            '<li><b>this.name</b> : name of the running bot</li>'+
            '<li><b>this.contents</b> : the bot\'s inventory</li>'+
            '<li><b>node.entities</b> : entities in the current node/location</li></ul>'+
            '<h2>Examples</h2> Say names of everything in our current node/location:'+
            '<ul>'+
            '<li>@MyBot 10 for e in node.entities</li>'+
            '<li>@MyBot 20 print e.name</li>'+
            '<li>@MyBot 30 end</li>'+
            '<li>@MyBot 40 goto 10</li></ul>'+
            'Search the bot\'s inventory:'+
            '<ul>'+
            '<li>@MyBot 10 for i in this.contents</li>'+
            '<li>@MyBot 20 if i.name is Key</li>'+
            '<li>@MyBot 30 say I have the key!</li>'+
            '<li>@MyBot 40 end</li>'+
            '<li>@MyBot 50 end</li>'+
            '<li>@MyBot 60 goto 10</li></ul>';


        var textD = '<b>pluto-node = room</b>'+
                '<ul><li>name (string)</li>'+
                '<li>info (string)</li>'+
                '<li>entities (list of entities)</li>'+
                '<li>messages (list of messages)</li>'+
                '<li>edges (list of connections to other rooms)</li></ul>'+
                '<b>message</b>'+
                '<ul><li>meaning (string, "spam" "important" ...)</li>'+
                '<li>from (string)</li>'+
                '<li>to (string)</li>'+
                '<li>txt (string)</li></ul>'+
                '<b>entity = players and bots</b>'+
                '<ul><li>id (number)</li>'+
                '<li>name (string)</li>'+
                '<li>desc (string)</li>'+
                '<li>contents (list)</li>'+
                '<li>face-furniture (entity)</li>'+
                '<li>hat (entity)</li>'+
                '<li>money (number)</li>'+
                '<li>criminal-rating (number)</li>'+
                '<li>likes (list)</li>'+
                '<li>liked-by (list)</li></ul>';

        $('#help-page').empty();

        if (page === 1)
        {
            $('#help-page').append(textA);
        }
        else if (page === 2)
        {
            $('#help-page').append(textB);
        }
        else if (page === 3)
        {
            $('#help-page').append(textC);
        }
        else if (page === 4)
        {
            $('#help-page').append(textD);
        }
}

$(document).ready(function ()
{
    help_page(1);

    var popup = $("#readme");
    var content = popup.children("#readme-content");
    var img = content.children("img");

    popup.css("display", "block").data("showing", false);

    // cursor update
    img.mouseover(function ()
    {
        $(this).css('cursor', 'pointer');
    });

    // slide in and out...
    img.click(function ()
    {
        if (popup.data("showing") === true)
        {
            popup.data("showing", false).animate(
            {
                marginLeft: "-655px"
            }, 500);
            $(this).attr("src", "images/help.png").css("top", "0px");
        }
        else
        {
            popup.data("showing", true).animate(
            {
                marginLeft: "0"
            }, 500);
            $(this).attr("src", "images/close.png").css("top", "0px");
        }
    });

});
