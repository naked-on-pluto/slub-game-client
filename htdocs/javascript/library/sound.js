var context;

var formants = 
 [[[700, 130], [1220,  70], [2500, 160], [3300, 250],[3750, 200],[4900, 1000]],
  [[480,  70], [1720, 100], [2520, 200], [3300, 250],[3750, 200],[4900, 1000]],
  [[310,  45], [2020, 200], [2960, 400], [3300, 250],[3750, 200],[4900, 1000]],
  [[550,  80], [960,   50], [2400, 130], [3300, 250],[3750, 200],[4900, 1000]],
  [[350,  65], [1250, 110], [2200, 140], [3300, 250],[3750, 200],[4900, 1000]]
 ];

var samples = ['gabbakick', 'bd', 'sn'];
var bufferLookup = {};

function Vocable() {
    this.node = context.createScriptProcessor(4096, 1, 1);
    this.impulse = -1;
    this.impulseStrength = 1;
    this.vowelnum = 0;
    this.stringSz = this.maxStringSz = Math.round(context.sampleRate / 100);
    this.y = new Float32Array(this.maxStringSz);
    this.n = 0;
    this.formant_history = new Array();
    for (var i = 0; i < 11; i++) {
        this.formant_history.push(0.0);
    }
    this.speaking = false;
    this.pause = 0;
    this.wordQueue = "";
    this.noisiness = 0;

    that = this;

    this.node.onaudioprocess = function(e) {
        var output = e.outputBuffer.getChannelData(0);
        for (var i = 0; i < output.length; i++) {
            var input = 0;
            with (that) {
                if (impulse >= 0) {
                    input = (Math.random()-0.5)*impulseStrength;
                    impulse--;
                }

                y[n] = input + (y[n] + y[(n + 1) % stringSz]) / 2.01;
                if (Math.random() < noisiness) {
		    y[n] = 0 - y[n];
      		}
                output[i] = y[n];
                n++;
                if (n >= stringSz) {
                    n = 0;
                }

                if (speaking) {
		    if (pause > 0) {
			pause--;
		    }
		    else {
			that.nextLetter();
		    }
		}
            }
        }
    }

    this.nextLetter = function() {
        if (this.wordQueue.length <= 0) {
	    speaking = false;
	}
	else {
            var letter = this.wordQueue.charAt(0);
            this.wordQueue = this.wordQueue.substring(1);
            this.pause = context.sampleRate / 10;
            this.noisiness = 0;
	    if (! isVowel(letter)) {
		this.setSz(1)
	    }
            switch(letter.toLowerCase()) {
            case 'a':
                useFormant(0);
		break;
            case 'e':
                useFormant(1);
		break;
            case 'i':
                useFormant(2);
		break;
            case 'o':
                useFormant(3);
		break;
            case 'u':
                useFormant(4);
		break;
            case 'b':
		this.trigger(2000);
		this.setSz(0.5);
		break;
            case 'c':
		this.trigger(2000);
		this.noisiness = 0.4;
		this.setSz(0.3);
		break;
            case 'd':
		this.trigger(2000);
		this.noisiness = 0.2;
		this.setSz(0.7);
		break;
            case 'f':
		this.trigger(2000);
		this.noisiness = 0.3;
		this.setSz(0.4);
		break;
            case 'g':
		this.trigger(2000);
		break;
            case 'h':
		this.trigger(2000);
		this.noisiness = 0.1;
		this.setSz(1);
		break;
            case 'j':
		this.trigger(2000);
		break;
            case 'k':
		this.trigger(2000);
		this.noisiness = 0.3;
		break;
            case 'l':
		this.trigger(2000);
		break;
            case 'm':
		this.trigger(2000);
		this.setSz(0.9);
		break;
            case 'n':
		this.trigger(2000);
		break;
            case 'p':
		this.trigger(2000);
		this.setSz(0.2);
		this.noisiness = 0.35;
		break;
            case 'q':
		this.trigger(2000);
		this.noisiness = 0.45;
		break;
            case 'r':
		this.trigger(2000);
		this.setSz(0.5);
		break;
            case 's':
		this.trigger(10, 0.25);
		this.noisiness = 0.5;
		break;
            case 't':
		this.trigger(1000,1);
		this.setSz(0.3);
		this.noisiness = 0.5;
                this.pause * 0.5;
		break;
            case 'v':
		this.trigger(2000);
		break;
            case 'w':
		this.trigger(2000);
            case 'x':
		this.noisiness = 0.5;
		this.trigger(2000);
            case 'y':
		this.trigger(2000);
            case 'z':
		this.noisiness = 0.1;
		this.setSz(0.6);
		this.trigger(2000);
            }
        }
    }

    this.connect = function(dest) {
        this.node.connect(dest);
    };
    this.trigger = function(duration,strength) {
        if (typeof(strength)==='undefined') {
	    strength = 0.5;
  	}
        this.impulse = context.sampleRate / duration;
        this.impulseStrength = strength;
    }

    this.say = function(word, reverb) {
	word = word.trim();
	if (word in bufferLookup) {
	    var source = context.createBufferSource();
	    source.buffer = bufferLookup[word];
	    source.loop = false;
	    source.connect(context.destination);
	    source.start(0);
	}
	else {
            this.wordQueue += word + " "
	    this.speaking = true;
	}
    }
    this.setSz = function(sz) {
	if (sz > 1) {
	    sz = 1;
	}
	if (sz < 0) {
	    sz = 0;
	}
	var foo = Math.floor(sz * this.maxStringSz);
	if (foo < this.stringSz) {
	    for (i = foo; i < this.stringSz; ++i) {
		this.y[i] = 0.0;
	    }
	}
	this.stringSz = foo;
    }
}

function isVowel(l) {
    if (['a', 'e', 'i', 'o', 'u'].indexOf(l) >= 0) {
	return(true);
    }
    return(false);
}

function useFormant(f) {
    if( typeof f == "number") {
      f = formants[f];
    }
    var F = 0;
    var BW = 1;
    for(var i = 0; i< formants[0].length; i++) {
      var formant = f[i];
      var filter = window.filters[i];
      filter.frequency.value = formant[F];

      var Q =  formant[F] / (formant[BW] / 2);
      filter.Q.value = Q;
    }
}

function loadBuffers() {
    for (var i = 0; i < samples.length; ++i) {
	var sample = samples[i];
	var request = new XMLHttpRequest();
	var url = "/samples/" + sample + ".wav";

	request.open("GET", url, true);
	request.responseType = "arraybuffer";
	
	var loader = this;
	// half-arsed language
	var f = function(sample){return function(e) {
	    var foo = sample;
            context.decodeAudioData(
		e.currentTarget.response,
		function(buffer) {
                    bufferLookup[foo] = buffer;
		}
            );
	}};

	request.onload = f(sample)

	request.onerror = function() {
	    alert('BufferLoader: XHR error');
	}
	
	request.send();
    }
}


$(document).ready(function () {
    context = new webkitAudioContext();
    window.voc = new Vocable();
    var filters = new Array();
    for(var i = 0; i < formants[0].length; i++) {
        var filter = context.createBiquadFilter();
        filter.type = filter.BANDPASS;
	window.voc.connect(filter);
        filter.connect(context.destination);
        filters.push(filter);
    }
    window.filters = filters;
    useFormant(0); // a

    loadBuffers();
});
