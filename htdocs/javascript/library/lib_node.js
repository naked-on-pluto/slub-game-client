/////////////////////////////////////////////////////////
// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function lib_node(name,src,factory,angle)
{
    this.name=name;
    this.factory=factory;
    this.filter=new node_filter();
    this.objects_gone=[];
    this.objects_arrived=[];
    this.angle=angle;

    this.has_object = function(id,objs)
    {
        for (var i in objs)
        {
            if (objs[i].id==id) return true;
        }
        return false;
    }

    this.update_objects_changed = function(old_objs,new_objs)
    {
        this.objects_gone=[];
        this.objects_arrived=[];

        for (var i in old_objs)
        {
            if (!this.has_object(old_objs[i].id,new_objs))
            {
                this.objects_gone.push(old_objs[i]);
            }
        }
        for (var i in new_objs)
        {
            if (!this.has_object(new_objs[i].id,old_objs))
            {
                this.objects_arrived.push(new_objs[i]);
            }
        }
    }

    this.update = function(src)
    {
        // don't do this first time
        if (this.src!=null)
        {
            this.update_objects_changed(this.src.objects,
                                        src.objects);
        }
        this.src=src;
        this.brx=this.factory.build_update
        (this.name,
         this.filter.filter(src));
    }

    this.draw = function(ctx,x,y)
    {
        ctx.save();
        ctx.translate(x,y);
/*        cx=x-library_centre_x;
        cy=y-library_centre_y;
        n=normalise([cx,cy])*/
        ctx.rotate(-this.angle);

//        ctx.rotate(Math.atan2(n[1],n[0])-Math.atan2(1,0));
        ctx.translate(-x,-y);
        this.brx.draw(ctx,x-brx_width,y+brx_height);
        ctx.restore();
    }

    this.do_exits_contain = function(name)
    {
        for (var i in this.src.exits)
        {
            if (this.src.exits[i].name==name) return true;
        }
        return false;
    }

    this.rnd_exit = function()
    {
        return choose(this.src.exits);
    }

    this.update(src);
}
