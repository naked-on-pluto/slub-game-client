/////////////////////////////////////////////////////////
// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function floating_msg(x,y,txt,angle)
{
    this.x=x;
    this.y=y;
    this.txt=txt;
    this.age=0;
    this.speed=0.6;
    this.angle=angle;
    this.lines=[];

    this.draw = function(ctx)
    {
        if (this.lines.length==0)
        {
            this.lines=wrap_text(ctx,this.txt,120);
        }
        
        ctx.save();
        ctx.translate(this.x,this.y);
        ctx.rotate(-this.angle);
        ctx.translate(-50,0);
//        ctx.scale(2,2);
        ctx.fillStyle = fg_colour;  
        
        var y=0;
        var c=0; 
        for (var i in this.lines)
        {
            var text=this.lines[i];
            var next_c=c+text.length;
            if (next_c>this.age)
            {
                text=text.substring(0,this.age-c);
            }

            if (c<this.age)
            {
                ctx.fillText(text,0,y);
            }
            c=next_c;
            y+=brx_height;
        }
        
        ctx.restore();
        this.age+=this.speed;
    }
}

function listener()
{
    this.msgs=[];
    this.max_msgs=5;

    this.exists = function(txt)
    {
        for (i in this.msgs)
        {
            if (this.msgs[i].txt==txt) return true;
        }
        return false;
    }
    
    this.update = function(messages)
    {
        if (messages!=null)
        {
            this.add(messages[0].txt);
        }
    }

    this.pick_free_angle = function()
    {
        var num_slots=6;
        var deg_per_slot=(3.141*2)/num_slots;
        while (true)
        {
            var angle=Math.floor(Math.random()*num_slots)*deg_per_slot;
            var test=true;
            for(var i in this.msgs)
            {
                if (this.msgs[i].angle==angle)
                {
                    test=false;
                    break;
                }
            }
            if (test) return angle;
        }
    }
    
    this.add = function(txt)
    {
        if (!this.exists(txt) &&
            this.msgs.length<this.max_msgs)
        {
            var a=this.pick_free_angle();
            var x=Math.sin(a)*120;
            var y=Math.cos(a)*120;
            
            this.msgs.push(new floating_msg(x+library_centre_x,
                                            y+library_centre_y,
                                            txt,
                                            a));
        }
    }

    this.draw = function(ctx)
    {
        for (var i in this.msgs)
        {
            this.msgs[i].draw(ctx);
        }
        
        var m=[];
        for (var i in this.msgs)
        {
            if (this.msgs[i].age<500)
            {
                m.push(this.msgs[i]);
            }
        }
        this.msgs=m;
    }
    
    this.spybot_check = function(src)
    {
        for (var i in src.objects)
        {
            if (src.objects[i].contents!=null)
            {
                var o=choose(src.objects[i].contents);
                if (o[2]=="Secret")
                {
                    this.add(src.objects[i].name + ": " + o[3]);
                }
            }
        } 
    }
}