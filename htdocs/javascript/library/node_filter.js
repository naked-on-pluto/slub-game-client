/////////////////////////////////////////////////////////
// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function node_filter()
{
    this.filter=function(obj)
    {
//        alert(JSON.stringify(obj));
	var r={};

	var objects=[];

	var reverb = false;
	var reverbpatt = /reverbunit/i;

	if (obj.objects!=null) {
            objects=obj.objects.map(function(object) {
                var wearing="";
                if (object.clothes) {
                    wearing=" wearing a "+object.clothes[0].name;
                }
		if (reverbpatt.test(object.name)) {
		    reverb = true;
		}
                return object.name;//+wearing;
            });
	}

	// add the messages from the node to the list of objects
	if (obj.messages!=null) {
            objects=objects.concat(obj.messages.map(function(msg) {
		// insert sound here - will have to check if it's already been said
		var isbot = false;
                var username = ""; 
		for (var i = 0; i < obj.objects.length; ++i) {
                    var e = obj.objects[i];
		    if (msg.from == e.id) {
			username = e.name;
			var patt = /bot\s*$/i;
			if (patt.test(username)) {
			    isbot = true;
			}
			break;
      		    }
                }
		msg.txt = msg.txt.replace(/\s+$/, '');
		if(isbot) {
                    voc.say(msg.txt, reverb);
		}
                return '"'+msg.txt+'"';
            }));
	}
        return objects;
    }
}
