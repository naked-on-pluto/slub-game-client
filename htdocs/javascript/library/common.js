/////////////////////////////////////////////////////////
// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.


function vadd(a,b)
{
    return [a[0]+b[0],a[1]+b[1]];
}

function normalise(v)
{
    var d=Math.sqrt(v[0]*v[0]+v[1]*v[1]);
    return [v[0]/d, v[1]/d];
}

function distance(a,b)
{
    var v=[a[0]-b[0],a[1]-b[1]];
    return Math.sqrt(v[0]*v[0]+v[1]*v[1]);
}

function contains(l,a)
{
    for (i in l)
    {
        if (l[i]==a) return true;
    }
    return false;
}

function choose(l)
{
    return l[Math.floor(Math.random()*l.length)];
}

// waiting for ecmascript version whatever
function object_keys(obj)
{
    var keys = [];
    for(var key in obj) {keys.push(key);}
    return keys;
}

// half baked damn language...
function object_size(obj)
{
    var size = 0;
    for(var key in obj) {size++;}
    return size;
}


function wrap_text(ctx,phrase,maxPxLength) {
    var wa=phrase.split(" "),
        phraseArray=[],
        lastPhrase="",
        l=maxPxLength,
        measure=0;
    for (var i=0;i<wa.length;i++) {
        var w=wa[i];
        measure=ctx.measureText(lastPhrase+w).width;
        if (measure<l) {
            if (lastPhrase!="")
            {
                lastPhrase+=(" "+w);
            }
            else
            {
                lastPhrase+=w;
            }
        } else {
            phraseArray.push(lastPhrase);
            lastPhrase=w;
        }
        if (i===wa.length-1) {
            phraseArray.push(lastPhrase);
            break;
        }
    }
    return phraseArray;
}