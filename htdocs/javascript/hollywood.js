// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

// Special effects from the Industry.
// But just for those just arriving

$(function() {

    // Browser compatibility test
    if (!$.browser.webkit && !$.browser.mozilla) 
    {
        alert("At the moment, EVr14 has been exclusively developed for the most recent versions of:\n\n  - webkit (Chromium, Chrome, Safari,...)\n  - mozilla (Firefox)\n\nIf you are not using one of the browsers listed above, this might lead to a disrupted customer experience.\n\nYours truly,\nPlutonian Corp.");
    }


    $("#title").css("visibility", "visible");
    $("#footer-intro").css("visibility", "visible");
    $("#accept").css("visibility", "visible");

    if (document.referrer.search('evr14.naked-on-pluto') == -1)
    {
        $("#title").css("opacity","0").delay(1000).animate({opacity: '1'}, 3000, function() {});
        $("#footer-intro").css("opacity", "0").delay(3000).animate({opacity: '1'}, 0, function() {});
        $("#accept").css("opacity", "0").delay(4000).animate({opacity: '1'}, 0, function() {});
    }
    else
    {
        $("#title").css("opacity","1");
        $("#footer-intro").css("opacity", "1");
        $("#accept").css("opacity", "1");
    }
});

function make_title()
{
    var title = document.URL.split('#')[1];
    if (title != undefined)
    {
	document.write(unescape(title.replace(/_/g,' '))+'®');
    }
    else
    {
	document.write('Naked on Pluto');
    }
}