// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//                                       
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

function message(txt)
{
    area.message.value+=txt+"\n";
}

function clear()
{
    area.message.value="";
}

function choose(arr)
{
    return arr[Math.floor(Math.random()*arr.length)];
}

function player(id)
{
    this.location = "start";
    this.ready = true;
    this.id = id;

    this.tick = function()
    {
        if (this.ready)
        {
            this.ready=false;
            server_call("get-node",{"name": this.location}, 
                        function (data) 
                        {  
                            var player = game.players[id];
                            var node = JSON.parse(data);
                            if (!node)
                            {
                                message("hmm, couldn't load "+name);
                            }
                            else
                            {                              
                                if (node.exits!=null)
                                {
                                    player.location=choose(node.exits).to;
                                    message("player "+player.id+" has moved to "+player.location);
                                }

                                player.ready=true;
                            }
                        }
                       ); 
        }
    }
}

function group(size)
{
    this.players=[];
    for (var i=0; i<size; i++)
    {
        this.players.push(new player(i));
    }

    this.tick = function()
    {
        for (var i in this.players)
        {
            this.players[i].tick();
        }
    }
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

var game = new group(500);

function go()
{
    game.tick();
    setTimeout("go();",100);
}

go();


