// Naked on Pluto Copyright (C) 2010 Aymeric Mansoux, Marloes de Valk, Dave Griffiths
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

function fb_interface(appid,refresh)
{
    this.refresh=refresh;
    this.friends=[];
    this.current_friend=-1;
    this.accessToken=false;
    this.uid=false;

    if (appid!="")
    {
        this.data = {
            me: {},
            likes: [],
            people: [],
            books: [],
	        movies: [],
            mylocations: [],
            locations: [],
            apps: [],
            peoplelikes: []
        }
    }
    else
    {
        this.current_friend=0;
        // some test data
        this.data = {
            me: { id:99, first_name:"Bob", gender:"male",
                  last_name:"Bloggs", name:"Bob Bloggs"},
            mypic: "",
            people: ["FredFoo","JimJobble","BobbyBoobar"],
            peoplelikes: [
                {name:"FredFoo",likes:["Jam", "Honey"]},
                {name:"JimJobble",likes:["Cars", "Flowers"]},
                {name:"BobbyBoobar", likes:["Red", "Green"]}
            ],
            apps: ["Naked on Pluto"],
            books: ["The Dictionary", "The DaVinci Code"],
	        movies: ["The Third Man"],
            mylocations: ["Sweet Home"],
            locations: ["Barcelona, Spain"],
            likes: ["One","Two","Three"]
        }

        $('#wrapper-game').css('display','block');
        $('#footer-game').css('display','inline');
//        $('#input').focus();

	    var that=this;
        var logged_in=false;
        var first_keypress=true;
        $('#input').val("Enter your name to enter slubworld");

        $('#input').click(function() {

            if (first_keypress) {
                $('#input').val("");
                first_keypress=false;
            }
        });

	    $('#input').keypress(function(e) {
            first_keypress=false;
            if(!logged_in && e.which == 13) {
                logged_in = true;
		        that.data.me.name=$('#input').val();
                $('#input').val("");
		        that.data.me.first_name=that.data.me.name;
		        that.data.me.last_name="";
		        that.data.me.id = that.data.me.name;

		        var bits=that.data.me.name.split(" ");
		        if (bits.length>1) {
		            that.data.me.first_name=bits[0];
		            that.data.me.last_name=bits[1];
		        }
		        console.log("logging in as "+that.data.me.id);

		        refresh("me",that.data.me);
	        }
        });

    }

    /////////////////////////////////////////////////////////////////
    // the login stuff

    this.login=function()
    {


        // Clean page from intro texts/menu
        $('#wrapper-intro').css('display','none');
        $('#footer-intro').css('display','none');
        // Show the loading screen
        newLoaderKeyword();
        $('html').css('background-color', '#3b5998');
        $('html').css('background-image', 'url(images/pluto_logo_transparent.png)');
        $('html').css('background-repeat', 'repeat');
        $("#loader").css("display","inline");

        var fb=this;

    }

    // ... and the logout
    this.logout=function()
    {
        FB.logout(function()
        {
            window.location="http://naked-on-pluto.net";
            // Turns game page into intro page
//            $('html').css('background-image', 'url(images/theatrical_opening.jpg)');
//            $('html').css('background-color', '#FFF');
//            $('#wrapper-game').css('display','none');
//            $('#wrapper-intro').css('display','block');
//            $('#footer-intro').css('display','inline');
        });
    };




    this.poll = function()
    {
        if (appid=="" && this.current_friend>-1)
        {
/*            clean_feeds();
            $('#loader').css('display','none');
            $('#wrapper-game').css('display','block');
            $('#footer-game').css('display','inline');
            $('#input').focus();
*/
//            this.current_friend=-1;
        }
    }

    //////////////////////////////////////////////////////////////////
    // stuff for getting data out

    // look for words like #something#xx# and replace with fb data
    this.substitute_fake_info = function(str)
    {
	var that = this;
	var spoofed = '';
	return str.replace(/(#[a-z]*#[0-9]*#[0-9]*#|#[a-z]*#[0-9]*#|#[a-z]*)/g, function(spoof)
        {
	        //if (spoof.indexOf('person'))
            //{
            //    return '@<span class="their entity">'+that.get_data_from_string(spoof)+'</span>';
            //}
            //else
            //{
                return that.get_data_from_string(spoof);
            //}
        });
    }

    this.safe_get = function(type,index)
    {
        var len=this.data[type].length;
        if (len>0) return this.data[type][index%len];
        else return "undefined!";
    }

    this.get_data_from_string = function(str)
    {
        var toks=quote_split(str,"#");
        var t=toks[1];
        if (t=="myid") return this.data.me.id;
        if (t=="myname") return this.data.me.name.replace(/\s/g, "");
        var i=parseInt(toks[2]);
        var d="";
        if (toks.length>2) d=toks[3];
        if (t=="person") return this.safe_get("people",i)+d;
        if (t=="mylocation") return this.safe_get("mylocations",i)+d;
        if (t=="location") return this.safe_get("locations",i)+d;
        if (t=="movie") return this.safe_get("movies",i)+d;
        if (t=="book") return this.safe_get("books",i)+d;
        if (t=="like") return this.safe_get("likes",i)+d;
        if (t=="app") return this.safe_get("apps",i)+d;
        if (t=="friendlike")
        {
            //alert(toks.length);
            var friend=this.safe_get("peoplelikes",i);
            if (toks.length==4)
            {
                return friend.name+d;
            }
            else
            {
                var likes=friend.likes;
                var y=parseInt(d);
                if (toks.length>3) d=toks[4];
                return likes[y%likes.length]+d;
            }

        }
     }

    //if (appid!="") this.init();
}
